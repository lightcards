---
title: lightcards
section: 1
header: General Commands Manual
footer: VERSION_HERE
date: February 2021
---

# NAME
lightcards - terminal flashcards from Markdown

# SYNOPSIS
lightcards \[options\] \[input files\]

# DESCRIPTION
**lightcards** is a Python program that reads data from two column Markdown (and by extension, HTML) tables from the **input files**, and displays flashcards from their contents.

# OPTIONS
## Generic Program Information
**-h**, **\--help**
: Show a help message and exit

**-v**, **\--version**
: Print version and exit

## Startup options
**-a**, **\--alphabetize**
: Alphabetize card order

**-p**, **\--purge**
: Purge cache for chosen set

**-r**, **\--reverse**
: Reverse card order

**-s**, **\--shuffle**
: Shuffle card order

**-V** \[1-3\], **\--view** \[1-3\]
: Specify startup view

**-c** \[config file\], **\--config** \[config file\]
: Specify a custom config file

**-l**, **\--lenient**
: Don't raise exception if tables are malformed

**-t** \[int\], **\--table** \[int\]
: If multiple tables are provided in the input files, specify which

# KEYS
**l**, **right**
: Next card

**h**, **left**
: Previous card

**j**, **k**, **up**, **down**
: Flip card

**i**, **/**
: Star card

**0**, **^**, **home**
: Go to start of deck

**$**, **end**
: Go to end of deck

**H**, **?**
: Open help screen

**m**
: Open control menu

**1**, **2**, **3**
: Switch views between front first, back first, and both

**q**
: Quit

# INPUT FILE
Lightcards takes the first table from a valid Markdown or HTML file. Each row is a card, and the two columns are the front and back.

# EXIT STATUS
**0**
: Success

**1**
: Parse error

**2**
: Invalid option

**3**
: Curses error

**4**
: Config error

# SEE ALSO
  **lightcards-config(5)**

# BUGS
https://lists.sr.ht/~armaan/public-inbox

# COPYRIGHT
Copyright 2021 Armaan Bhojwani <me@armaanb.net>, MIT License.
