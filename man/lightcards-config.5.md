---
title: lightcards-config
section: 5
header: File Formats Manual
footer: VERSION_HERE
date: February 2021
---

# NAME
lightcards-config - configuration file formats for **lightcards(1)**

# CONFIGURATION
Configuration is done through the config.py file. This is an executed Python script and must have valid Python syntax. Whilst this does have some drawbacks of being stricter on syntax, it also allows you to do things such as appending values to lists instead of overwriting them, and using logic within the configuration through actual Python code.

Copy the global config file from /etc/lightcards/config.py to ~/.config/lightcards/config.py or $XDG_CONFIG_HOME/lightcards/config.py and make modifications! All possible options are listed in the global config file.

# SEE ALSO
  **lightcards(1)**
