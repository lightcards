| Side 1       | Side 2      | Side 3       |
|--------------|-------------|--------------|
| Card 1 Front | Card 1 Back | Card 1 Third |
| Card 2 Front | Card 2 Back | Card 2 Third |
| Card 3 Front | Card 3 Back | Card 3 Third |
| Card 4 Front | Card 4 Back | Card 4 Third |
| Card 5 Front | Card 5 Back | Card 5 Third |
| Card 6 Front | Card 6 Back | Card 6 Third |
| Card 7 Front | Card 7 Back | Card 7 Third |
| Card 8 Front | Card 8 Back | Card 8 Third |
