###############################################################################
# LIGHTCARDS DEFAULT CONFIG FILE                                              #
###############################################################################
import curses

###############################################################################
# STARTUP OPTIONS

alphabetize = False
shuffle = False
reverse = False
cache = True
table = False  # set to integer or range
lenient = False

default_view = 1

###############################################################################
# APPEARANCE

progress_char = "»"

highlight_color = curses.COLOR_CYAN
starred_color = curses.COLOR_YELLOW
error_color = curses.COLOR_RED

###############################################################################
# KEYBINDINGS

card_prev = ["h", "KEY_LEFT"]
card_next = ["l", "KEY_RIGHT"]
card_flip = ["j", "k", "KEY_UP", "KEY_DOWN"]
card_star = ["i", "/"]
card_first = ["0", "^", "KEY_HOME"]
card_last = ["$", "KEY_END"]

help_disp = ["H", "?"]

menu_disp = "m"
menu_reset = "y"
menu_alphabetize = "a"
menu_shuffle = "z"
menu_reverse = "t"
menu_unstar = "u"
menu_star = "d"
menu_stars_only = "s"
menu_open_file = "e"
menu_restart = "r"

view_one = "1"
view_two = "2"
view_three = "3"

quit_key = "q"

###############################################################################
# OTHER

quit_confirmation = True
show_menu_at_end = True
debug = False

###############################################################################
